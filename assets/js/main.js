"use strict";
// DOM elements
const displayElement = document.querySelector(".display-number");
const clearElement = document.querySelector(".clear");
const equalsElement = document.querySelector(".equals");
const backspaceElement = document.querySelector(".backspace");
const btnContainer = document.querySelector(".btn-container");

let stack = [];
const operators = ["+", "-", "*", "/"];
let isPeriodUsed = false;

// event listener for pressing key and operators and validating it
btnContainer.addEventListener("click", (event) => {
  if (event.target.value) {
    // for initial operator that cant be *,/
    if (!isValid(event.target.value)) {
      return;
    }

    stack.push(event.target.value);
    displayElement.value = stack.join("");
  }
});

backspaceElement.addEventListener("click", () => {
  stack.pop();
  displayElement.value = stack.join("");
});

// event listener for clearing display(CE) funtionality
clearElement.addEventListener("click", () => {
  displayElement.value = "";
  stack = [];
  isPeriodUsed = false;
});

// event listener for getting result
equalsElement.addEventListener("click", () => {
  // const result = calculate(stack.join(""));
  const result = calculate();
  displayElement.value = result;
  if (displayElement.value.includes(".")) {
    isPeriodUsed = true;
  }
  stack = [];
  stack.push(result);
});

let result = 0;
function calculate() {
  const splittedArray = splitNumberAndOperator(stack.join(""));

  if (operators.includes(splittedArray[0])) {
    let temp = splittedArray.shift();
    splittedArray[0] = temp + splittedArray[0];
  }

  let result = parseFloat(splittedArray[0]); // Initialize result with the first operand

  for (let i = 1; i < splittedArray.length; i += 2) {
    const operator = splittedArray[i];
    const operand = parseFloat(splittedArray[i + 1]);

    switch (operator) {
      case "+":
        result += operand;
        break;
      case "-":
        result -= operand;
        break;
      case "*":
        result *= operand;
        break;
      case "/":
        result /= operand;
        break;
    }
  }

  return parseFloat(result.toFixed(10));
}

function isValid(clickedKey) {
  if (
    (stack.length === 0 && ["*", "/", "."].includes(clickedKey)) ||
    (stack.length === 1 &&
      stack[stack.length - 1] === "-" &&
      ["*", "/"].includes(clickedKey))
  ) {
    return false;
  }

  // for consecutive operators
  if (
    operators.includes(stack[stack.length - 1]) &&
    operators.includes(clickedKey)
  ) {
    stack.pop();
  }

  if (operators.includes(clickedKey)) {
    isPeriodUsed = false;
  }

  if (isPeriodUsed && clickedKey === ".") {
    return false;
  }

  if (clickedKey === ".") {
    isPeriodUsed = true;
  }

  return true;
}

function splitNumberAndOperator(input) {
  // Define a regular expression to match numbers and operators
  var regex = /(\d+(\.\d+)?|\+|\-|\*|\/)/g;

  // Use the match method to find all matches in the input string
  var matches = input.match(regex);

  if (matches) {
    // Return the array of numbers and operators
    return matches;
  } else {
    // If no matches found, return null or handle the case as needed
    return null;
  }
}
